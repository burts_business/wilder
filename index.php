<?php get_header(); ?>

	   <section class="cd-section copy shadow">
        <div class="title section">
	        <img id="services" class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>What We Do</h2>
	        <p>Taking the pressure off you so you can enjoy your day</p>
	        <p>Wild & Yellow exists to ensure that during those special occasions when you desperately need everything to go perfect, that it will. Easy going yet methodical, we develop each event with care and precision. In the planning stages we’d love to meet with you to make sure you have all the necessary boxes ticked for the big day. Then on the day we are committed to bringing your event to life, leaving you to enjoy, knowing it’s in capable hands. Plus, it’s always nice to hear from someone who's been there and done that don’t you think.</p>
	        <p>Let us shout you a coffee and help map out your event.</p>
	        <a href="#contact">LET’S GET TOGETHER</a>
	    </div>
    </section>
    
    <section id="group3" class="cd-section" data-parallax="scroll" data-speed="0.2" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-second.jpg">
    </section>
    
      <section class="cd-section copy">
		<div class="title section">
		    <img id="event" class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
		    <h2>Your Event</h2>
		    <p>Making sure everything is stunning and timed to perfection</p>
		    <p>Your wedding, gran’s 90th birthday or the office Christmas party. We know each event is a unique experience and has a different ‘set of rules’. No matter what you have planned, we bring our experience and creativity to the table whilst making sure everything is perfect when it needs to be. Allow us to help your vision reach its full potential and make sure your event is one for the memory banks. To make this process as easy as possible, as a rough indication, we charge $620 for all day event management and $90 per hour for consultation work.</p>
		    <a href="#contact">Let's Talk</a>
		</div>
    </section>
 
    
    <section id="group5" class="cd-section shadow" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-third.jpg">
    </section>
    
      <section class="cd-section copy">
        <div class="title section">
	        <img id="inspiration" class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>Inspiration</h2>
	        <p>Insight into our influences</p>
	        <p>Take a peek into Wild & Yellow’s well of inspiration; a bubbling oasis of travel, beautiful design and fantastically delicious & healthy food. We are inspired by the places we go, the people we meet and the experiences we share. Surprise and delight can be found at every corner; and it’s this approach that helps us bring a fresh and exciting atmosphere to any event.</p>
	        <a href="https://www.pinterest.com/hello4825/">Our Favourite Things</a>
	    </div>
    </section>
  
    
    <section id="group7" class="cd-section shadow" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-fourth.jpg">   
    </section>
    
      <section class="cd-section copy shadow">
        <div class="title section">
	        <img id="about" class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>About Us</h2>
	        <p>We love creating moments for you to remember</p>
	        <p>We are Wild & Yellow, a bespoke event hosting agency founded by Melis and Ash. Coming from backgrounds in events and marketing, we have found an occupation that makes our souls happy - helping others create amazing memories.</p><p>With a relaxed approach to our work, Melis spends her downtime doing anything healthy and Ash loves escaping to the ocean and cuddles with her cat Valentine.</p>
	        
	        <a href="#contact">Contact Us</a>
	    </div>
	</section>
    
    
    <section id="group9" class="cd-section shadow" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-seventh.jpg">
    </section>
    
	 <section class="cd-section copy">
		<div class="title section">
	        <img id="testimonial" class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>Testimonial</h2>
	        <p>A few words from some of our friends</p>
	        <p>Wild & Yellow managed our wedding day – the wedding ceremony and reception was at the same venue with 100 guests and a live band. Wild & Yellow’s organisational skills, attention to detail and forward thinking meant that as the bride I could relax and enjoy my day. Wild & Yellow quietly liaised with all other parties involved on the day with total respect and sincerity. I would totally recommend using Wild & Yellow’s services; they were instrumental in us enjoying such a beautiful and romantic day.</p> 
	        <i>Adele Keane</i>

			<!--<p>I really loved working with the girls at Wild & Yellow. I thought I had a clear plan of how I wanted my big day to go but after meeting with them, I realised how many details I had actually overlooked so I was so stoked for their help and my day was a huge success thanks to them. Money well worth spending.</p> 
			<i>Cassey Mitchell</i>-->

	        <p></p>
	        <a href="#contact">Reserve a time</a>
	    </div>
    </section>
  
   
    <section id="group11" class="cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-lead.jpg">
    </section>
    
    <section class="cd-section signup">
	    <div class="title section">
	        <h2>Join the Wild & Yellow Community</h2>
	        <div class="sign-up">
		        <p>For all the latest updates, inspiration and deals make sure you add your email below</p>
				<?php echo do_shortcode('[contact-form-7 id="4" title="Database Signup"]'); ?>
			</div>
	    </div>

    </section>
    
    <section class="cd-section contact">
        <div class="title section">
	        <img id="contact" class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>Contact Us</h2>
	        <p>Whether it’s over a glass of wine or a steaming English breakfast tea, we’d love to sit down with you to talk about your upcoming event. If you’re like the rest of us, you’re probably planning this event in your spare time so please don't hesitate to call after hours or over the weekend!</p>
	        <h3>Phone:</h3>
			<a href="tel:+64 21 026 09500">+64 21 026 09500</a>
	        <h3>Email:</h3>
	        <a href="mailto:hello@wildandyellow.com">hello@wildandyellow.com</a>
	        <div class="social">
		        <a href="mailto:hello@wildandyellow.com"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/email.svg" alt="Email"/></a>
		        <a href="https://www.facebook.com/Wild-Yellow-984745578242481/timeline/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="Facebook"/></a>
		        <a href="https://instagram.com/wild_and_yellow/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/instagram.svg" alt="Instagram"/></a>
		        <a href="https://www.pinterest.com/hello4825/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/pinterest.svg" alt="Pintrest"/></a>
	        </div>
	    </div>
    </section>
    
<?php get_footer(); ?>
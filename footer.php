			<section id="footer" class="">
		      <div>
			      <img src="<?php bloginfo('stylesheet_directory'); ?>/images/footer-lines.svg" />
			        <footer class="clear">
						<div class="footer-content">
							<a  style="margin-bottom:0px !important; font-weight: 100;" href="http://www.frankandpeggy.com/" target="_blank">Photography by Frank & Peggy</a>
							<p>&#169; Copyright Wild &amp; Yellow. <?php echo date("Y") ?></p>
							<a class="blink" href="http://blinkltd.co.nz/" target="_blank">
								handmade by blink.
							</a>
						</div>
					</footer>		        
		      </div>
			</section>
			
		</div><!--main content-->
	
	<a class="exit-off-canvas"></a>

  </div><!--innnerwrap-->
</div><!--off canvas wrap-->


	
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300' rel='stylesheet' type='text/css'>

	
<?php wp_footer(); ?>


</body>
</html>